//set up Sensor Suhu
#include "DHT.h"
#define DHTPIN 2     // what digital pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
const long interval = 10000;
DHT dht(DHTPIN, DHTTYPE);

//Setup WIFI
#include <ESP8266WiFi.h>

#ifndef STASSID
#define STASSID "Salsa"
#define STAPSK "salsa@123"
#endif

#ifndef STASSIDAP
#define STASSIDAP "suhu-data-log"
#define STAPSKAP "suhu@123"
#endif

//setup acces point 
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

const char* ssidAP = STASSIDAP;
const char* passwordAP = STAPSKAP;

const char* ssid = STASSID;
const char* password = STAPSK;

const char* host = "192.168.100.105";
const uint16_t port = 8000;
// const int httpPort = 80;

//setup web server
ESP8266WebServer server(80);

//web socket
#include <WebSocketsServer.h>
#include <Hash.h>
WebSocketsServer webSocket = WebSocketsServer(81);

void setup() {
  delay(5000);

  Serial.begin(115200);

  Serial.println(F("Begin Setup Suhu"));
  dht.begin();

  if(!wifiConnect(50000)){
    Serial.println("Failed Connect to Network ");
  }

  delay(1000);
  Serial.println("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  // WiFi.softAP(ssidAP, passwordAP);
  WiFi.softAP(ssidAP);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.begin();
  Serial.println("HTTP server started");

  startWebSocket(); //StartWeb Socket

}

bool wifiConnect(int timeout)
{
  uint64_t start = millis();

  Serial.println("Try Connect to Wifi ");
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);



  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");

    if (millis() - start > timeout)
		{
			WiFi.disconnect();
			return false;
		}
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  return true;

}

unsigned long prevTime = 0;

void loop() {
  
  webSocket.loop(); 
  //calculate interval read data
  unsigned long currTime=millis();
  unsigned long diff=currTime-prevTime;
  if(diff >= interval){
     DataLog(); 
     prevTime=currTime;
  }
    server.handleClient();
}

void handleRoot() {
  float humadity = dht.readHumidity();   //Read Homadity
  // float humadity = 40;   //Read Homadity
  float tempC = dht.readTemperature(); // Read temperature as Celsius (the default)
  // float tempC = 20; // Read temperature as Celsius (the default)
  float tempF = dht.readTemperature(true); // Read temperature as Fahrenheit (isFahrenheit = true)
  

    if (isnan(humadity) || isnan(tempC) || isnan(tempF)) {
      Serial.println("Failed to read from DHT sensor!");
      server.send(200, "text/html", "<h1> Failed to Read Data Sensor !!!!, Please Reconnect Module !!!!!!!!!</h1>");
      return ;
    }else{
        // Compute heat index in Fahrenheit (the default)
      float hiF = dht.computeHeatIndex(tempF, humadity);
      // Compute heat index in Celsius (isFahreheit = false)
      float hiC = dht.computeHeatIndex(tempC, humadity, false);


      String html = "<style> table, th, td { border: 1px solid black; border-collapse: collapse } </style><table>";
      html+="<thead> <tr> ";
      html+="<th>Humadity </th>";
      html+="<th>Temperature Celcius</th>";
      html+="<th>Temperature Fahrenheit </th>";
      html+="<th>Heat Index Celcius </th>";
      html+="<th>Heat Index Fahrenheit</th></tr></thead>";
      html+="<tbody>";
      html+="<tr>";
      html+="<td>";
      html+=humadity;
      // html+=" %</td>";
      html+=" %</td>";
      html+="<td>";
      html+=tempC;
      // html+=F("°C ");
      html+="</td>";
      html+="<td>";
      html+=tempF;
      // html+=F("°F ");
      html+="</td>";
      html+="<td>";
      html+=hiC;
      // html+=F("°C ");
      html+="</td>";
      html+="<td>";
      html+=hiF;
      // html+=F("°F");
      html+="</td>";
      html+="</tr>";
      html+="</tbody>";
      html+="</table>";

      server.send(200, "text/html", html);
    }


}


void DataLog(){
  Serial.println("Read Temperatur and Humadity");
  float humadity = dht.readHumidity();   //Read Homadity
  // float humadity = 40;   //Read Homadity
  float tempC = dht.readTemperature(); // Read temperature as Celsius (the default)
  // float tempC = 20; // Read temperature as Celsius (the default)
  float tempF = dht.readTemperature(true); // Read temperature as Fahrenheit (isFahrenheit = true)
  // float tempF = 75; // Read temperature as Fahrenheit (isFahrenheit = true)

  if (isnan(humadity) || isnan(tempC) || isnan(tempF)) {
    Serial.println("Failed to read from DHT sensor!");
    return ;
  }

  // Compute heat index in Fahrenheit (the default)
  float hiF = dht.computeHeatIndex(tempF, humadity);
  // Compute heat index in Celsius (isFahreheit = false)
  float hiC = dht.computeHeatIndex(tempC, humadity, false);

  Serial.print(F("Humidity: "));
  Serial.print(humadity);
  Serial.print(F("%  Temperature: "));
  Serial.print(tempC);
  Serial.print(F("°C "));
  Serial.print(tempF);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hiC);
  Serial.print(F("°C "));
  Serial.print(hiF);
  Serial.println(F("°F"));

  Serial.print("connecting to : ");
  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;

  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return;
  }

  Serial.println("connection success try to send data ");

  // We now create a URI for the request
  String url = "/api/temperatures";
  String data= "humadity=";
  data+=humadity;
  data+="&temp_c=";
  data+=tempC;
  data+="&temp_f=";
  data+=tempF;
  data+="&heat_index_c=";
  data+=hiC;
  data+="&heat_index_f=";
  data+=hiF;
  data+="&location=Lemari ES";
  data+="&ip=";
  data+=WiFi.localIP().toString();

  Serial.print("Data : ");
  Serial.println(data);

  client.println(String("POST "+url+" HTTP/1.1"));
  client.println(String("Host: ")+host+":"+port);
  client.println("Cache-Control: no-cache");
  client.println("Content-Type: application/x-www-form-urlencoded");
  client.print("Content-Length: ");
  client.println(data.length());
  client.println();
  client.println(data);

  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    // Serial.print(line);
  }
  
}

void startWebSocket() { // Start a WebSocket server
  webSocket.begin();                          // start the websocket server
  webSocket.onEvent(webSocketEvent);          // if there's an incomming websocket message, go to function 'webSocketEvent'
  Serial.println("WebSocket server started.");
}


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", num);
            break;
        case WStype_CONNECTED:
            {
                IPAddress ip = webSocket.remoteIP(num);
                Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        
                // send message to client
                webSocket.sendTXT(num, "Connected");
            }
            break;
        case WStype_TEXT:
            Serial.printf("[%u] get Text: %s\n", num, payload);

            // send message to client
//             webSocket.sendTXT(num, "message here");

            // send data to all connected clients
            String JSON=ReadTemp();
             webSocket.broadcastTXT(JSON);
            break;
    }
}

String ReadTemp(){
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float humadity = dht.readHumidity();   //Read Homadity
  // float humadity = 40;   //Read Homadity
  float tempC = dht.readTemperature(); // Read temperature as Celsius (the default)
  // float tempC = 20; // Read temperature as Celsius (the default)
  float tempF = dht.readTemperature(true); // Read temperature as Fahrenheit (isFahrenheit = true)
  // float tempF = 75; // Read temperature as Fahrenheit (isFahrenheit = true)

  if (isnan(humadity) || isnan(tempC) || isnan(tempF)) {
    Serial.println("Failed to read from DHT sensor!");
    String json = "{\"sensor\":\"dht\",\"status\":false,\"location\":\"Server MB Bitung\",\"data\":{\"humadity\":0,\"tempC\":0,\"tempF\": 0,\"hiC\": 0,\"hiF\": 0}}";
    return json;
  }

  // Compute heat index in Fahrenheit (the default)
  float hiF = dht.computeHeatIndex(tempF, humadity);
  // Compute heat index in Celsius (isFahreheit = false)
  float hiC = dht.computeHeatIndex(tempC, humadity, false);

  String json = "{\"sensor\":\"dht\",\"status\":true,\"location\":\"Lemari ES\",\"data\":{\"humadity\":"+String(humadity)+",\"tempC\":"+String(tempC)+",\"tempF\":"+String(tempF)+",\"hiC\":"+String(hiC)+",\"hiF\":"+String(hiF)+"}}";

  return json;
}


